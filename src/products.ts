import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new product into the database...")
    const productRepository = AppDataSource.getRepository(Product);
    const product = new Product()


    console.log("Loading products from the database...")
    const products = await productRepository.find()
    console.log("Loaded products: ", products)

    const updatedProduct = await productRepository.findOneBy({id: 1})
    
    updatedProduct.price = 45;
    console.log(updatedProduct)
    await productRepository.save(updatedProduct)

}).catch(error => console.log(error))
